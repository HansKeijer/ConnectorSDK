/*    SampleRSSourceData.sql                                                                  */
/*                                                                                            */
/*    This script populates the Scribe sample RS source database database with sample data.   */

use "ScribeSampleRSSource"
go

/* Delete all existing data */
delete from dbo.SalesOrderDetails;
delete from dbo.SalesOrders;
delete from dbo.Addresses;
delete from dbo.Customers;
delete from dbo.ProductPriceLists;
delete from dbo.Products;
delete from dbo.PickLists;
delete from dbo.GeneralLedger;
go


/* Load PickList values */
/* 	Order Types                                                                               */
insert into PickLists (PickListName, Code, Description) values ('OrderType', 'Quote', null);
insert into PickLists (PickListName, Code, Description) values ('OrderType', 'Order', null);
insert into PickLists (PickListName, Code, Description) values ('OrderType', 'Invoice', null);
go

/* 	Order Status                                                                              */
insert into PickLists (PickListName, Code, Description) values ('OrderStatus', 'Draft', null);
insert into PickLists (PickListName, Code, Description) values ('OrderStatus', 'Open', null);
insert into PickLists (PickListName, Code, Description) values ('OrderStatus', 'Closed', null);
insert into PickLists (PickListName, Code, Description) values ('OrderStatus', 'Posted', null);
insert into PickLists (PickListName, Code, Description) values ('OrderStatus', 'Void', null);
go

/* 	Units of Measure & Schedules                                                              */
insert into PickLists (PickListName, Code, Description) values ('UoMSchedules', 'General', null);
insert into PickLists (PickListName, Code, Description) values ('UoMSchedules', 'Length', null);
insert into PickLists (PickListName, Code, Description) values ('UoMSchedules', 'Consulting', null);
insert into PickLists (PickListName, Code, Description) values ('UoMSchedules', 'Support', null);
insert into PickLists (PickListName, Code, Description) values ('UoMSchedules', 'Warranty', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-General', 'Each', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-General', 'Pair', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-General', 'Case', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Length', 'Foot', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Length', 'Yard', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Length', 'Spool', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Consulting', 'Hour', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Consulting', 'Day', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Consulting', 'Week', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Consulting', 'Month', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Support', 'Quarter', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Support', 'Year', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Warranty', '1 Year', null);
insert into PickLists (PickListName, Code, Description) values ('UoM-Warranty', '3 Year', null);
go

/* 	Price Lists                                                                               */
insert into PickLists (PickListName, Code, Description) values ('PriceLists', 'Retail', null);
insert into PickLists (PickListName, Code, Description) values ('PriceLists', 'Wholesale', null);
go

/* 	Product Types                                                                             */
insert into PickLists (PickListName, Code, Description) values ('ProductTypes', 'FinishGood', null);
insert into PickLists (PickListName, Code, Description) values ('ProductTypes', 'Part', null);
insert into PickLists (PickListName, Code, Description) values ('ProductTypes', 'Subassembly', null);
insert into PickLists (PickListName, Code, Description) values ('ProductTypes', 'Service', null);
insert into PickLists (PickListName, Code, Description) values ('ProductTypes', 'Other', null);
go

/* 	Shipping Methods                                                                          */
insert into PickLists (PickListName, Code, Description) values ('ShippingMethods', 'FedEx-1day', null);
insert into PickLists (PickListName, Code, Description) values ('ShippingMethods', 'FedEx-2day', null);
insert into PickLists (PickListName, Code, Description) values ('ShippingMethods', 'UPS-Ground', null);
insert into PickLists (PickListName, Code, Description) values ('ShippingMethods', 'UPS-2day', null);
insert into PickLists (PickListName, Code, Description) values ('ShippingMethods', 'USPS', null);
go

/* 	Tax Schedules                                                                            */
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-MA', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-NH', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-NY', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-NJ', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-CT', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-VT', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-ME', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-RI', null);
insert into PickLists (PickListName, Code, Description) values ('TaxSchedules', 'ST-TX', null);
go

/* 	Address Types                                                                             */
insert into PickLists (PickListName, Code, Description) values ('AddressTypes', 'Main', null);
insert into PickLists (PickListName, Code, Description) values ('AddressTypes', 'BillTo', null);
insert into PickLists (PickListName, Code, Description) values ('AddressTypes', 'ShipTo', null);
insert into PickLists (PickListName, Code, Description) values ('AddressTypes', 'Other', null);
go

/* 	Payment Terms                                                                             */
insert into PickLists (PickListName, Code, Description) values ('PaymentTerms', 'Prepaid', null);
insert into PickLists (PickListName, Code, Description) values ('PaymentTerms', 'COD', null);
insert into PickLists (PickListName, Code, Description) values ('PaymentTerms', 'Net10', null);
insert into PickLists (PickListName, Code, Description) values ('PaymentTerms', 'Net15', null);
insert into PickLists (PickListName, Code, Description) values ('PaymentTerms', 'Net30', null);
insert into PickLists (PickListName, Code, Description) values ('PaymentTerms', 'Net45', null);
go

/* 	Region                                                                                    */
insert into PickLists (PickListName, Code, Description) values ('Region', 'North', 'North');
insert into PickLists (PickListName, Code, Description) values ('Region', 'Northeast', 'Northeast');
insert into PickLists (PickListName, Code, Description) values ('Region', 'Southeast', 'Southeast');
insert into PickLists (PickListName, Code, Description) values ('Region', 'Midwest', 'Midwest');
insert into PickLists (PickListName, Code, Description) values ('Region', 'Mid-south', 'Mid-south');
insert into PickLists (PickListName, Code, Description) values ('Region', 'Northwest', 'Northwest');
insert into PickLists (PickListName, Code, Description) values ('Region', 'South', 'South');
insert into PickLists (PickListName, Code, Description) values ('Region', 'Southwest', 'Southwest');
go

/* 	State                                                                                     */
insert into PickLists (PickListName, Code, Description) values ('State', 'AB','Alberta');
insert into PickLists (PickListName, Code, Description) values ('State', 'AK','Alaska');
insert into PickLists (PickListName, Code, Description) values ('State', 'AL','Alabama');
insert into PickLists (PickListName, Code, Description) values ('State', 'AR','Arkansas');
insert into PickLists (PickListName, Code, Description) values ('State', 'AS','American Samoa');
insert into PickLists (PickListName, Code, Description) values ('State', 'AZ','Arizona');
insert into PickLists (PickListName, Code, Description) values ('State', 'BC','British Columbia');
insert into PickLists (PickListName, Code, Description) values ('State', 'CA','California');
insert into PickLists (PickListName, Code, Description) values ('State', 'CO','Colorado');
insert into PickLists (PickListName, Code, Description) values ('State', 'CT','Connecticut');
insert into PickLists (PickListName, Code, Description) values ('State', 'DC','District of Columbia');
insert into PickLists (PickListName, Code, Description) values ('State', 'DE','Delaware');
insert into PickLists (PickListName, Code, Description) values ('State', 'FL','Florida');
insert into PickLists (PickListName, Code, Description) values ('State', 'FM','Federated States of Micronesia');
insert into PickLists (PickListName, Code, Description) values ('State', 'GA','Georgia');
insert into PickLists (PickListName, Code, Description) values ('State', 'GU','Guam');
insert into PickLists (PickListName, Code, Description) values ('State', 'HI','Hawaii');
insert into PickLists (PickListName, Code, Description) values ('State', 'IA','Iowa');
insert into PickLists (PickListName, Code, Description) values ('State', 'ID','Idaho');
insert into PickLists (PickListName, Code, Description) values ('State', 'IL','Illinois');
insert into PickLists (PickListName, Code, Description) values ('State', 'IN','Indiana');
insert into PickLists (PickListName, Code, Description) values ('State', 'KS','Kansas');
insert into PickLists (PickListName, Code, Description) values ('State', 'KY','Kentucky');
insert into PickLists (PickListName, Code, Description) values ('State', 'LA','Louisiana');
insert into PickLists (PickListName, Code, Description) values ('State', 'MA','Massachusetts');
insert into PickLists (PickListName, Code, Description) values ('State', 'MB','Manitoba');
insert into PickLists (PickListName, Code, Description) values ('State', 'MD','Maryland');
insert into PickLists (PickListName, Code, Description) values ('State', 'ME','Maine');
insert into PickLists (PickListName, Code, Description) values ('State', 'MH','Marshall Islands');
insert into PickLists (PickListName, Code, Description) values ('State', 'MI','Michigan');
insert into PickLists (PickListName, Code, Description) values ('State', 'MN','Minnesota');
insert into PickLists (PickListName, Code, Description) values ('State', 'MO','Missouri');
insert into PickLists (PickListName, Code, Description) values ('State', 'MP','Northern Mariana Islands');
insert into PickLists (PickListName, Code, Description) values ('State', 'MS','Mississippi');
insert into PickLists (PickListName, Code, Description) values ('State', 'MT','Montana');
insert into PickLists (PickListName, Code, Description) values ('State', 'NB','New Brunswick');
insert into PickLists (PickListName, Code, Description) values ('State', 'NC','North Carolina');
insert into PickLists (PickListName, Code, Description) values ('State', 'ND','North Dakota');
insert into PickLists (PickListName, Code, Description) values ('State', 'NE','Nebraska');
insert into PickLists (PickListName, Code, Description) values ('State', 'NF','Newfoundland');
insert into PickLists (PickListName, Code, Description) values ('State', 'NH','New Hampshire');
insert into PickLists (PickListName, Code, Description) values ('State', 'NJ','New Jersey');
insert into PickLists (PickListName, Code, Description) values ('State', 'NM','New Mexico');
insert into PickLists (PickListName, Code, Description) values ('State', 'NS','Nova Scotia');
insert into PickLists (PickListName, Code, Description) values ('State', 'NT','Northwest Territories');
insert into PickLists (PickListName, Code, Description) values ('State', 'NV','Nevada');
insert into PickLists (PickListName, Code, Description) values ('State', 'NY','New York');
insert into PickLists (PickListName, Code, Description) values ('State', 'OH','Ohio');
insert into PickLists (PickListName, Code, Description) values ('State', 'OK','Oklahoma');
insert into PickLists (PickListName, Code, Description) values ('State', 'ON','Ontario');
insert into PickLists (PickListName, Code, Description) values ('State', 'OR','Oregon');
insert into PickLists (PickListName, Code, Description) values ('State', 'PA','Pennsylvania');
insert into PickLists (PickListName, Code, Description) values ('State', 'PE','Prince Edward Island');
insert into PickLists (PickListName, Code, Description) values ('State', 'PR','Puerto Rico');
insert into PickLists (PickListName, Code, Description) values ('State', 'QC','Quebec');
insert into PickLists (PickListName, Code, Description) values ('State', 'RI','Rhode Island');
insert into PickLists (PickListName, Code, Description) values ('State', 'SC','South Carolina');
insert into PickLists (PickListName, Code, Description) values ('State', 'SD','South Dakota');
insert into PickLists (PickListName, Code, Description) values ('State', 'SK','Saskatchewan');
insert into PickLists (PickListName, Code, Description) values ('State', 'TN','Tennessee');
insert into PickLists (PickListName, Code, Description) values ('State', 'TX','Texas');
insert into PickLists (PickListName, Code, Description) values ('State', 'UT','Utah');
insert into PickLists (PickListName, Code, Description) values ('State', 'VA','Virginia');
insert into PickLists (PickListName, Code, Description) values ('State', 'VI','Virgin Islands');
insert into PickLists (PickListName, Code, Description) values ('State', 'VT','Vermont');
insert into PickLists (PickListName, Code, Description) values ('State', 'WA','Washington');
insert into PickLists (PickListName, Code, Description) values ('State', 'WI','Wisconsin');
insert into PickLists (PickListName, Code, Description) values ('State', 'WV','West Virginia');
insert into PickLists (PickListName, Code, Description) values ('State', 'WY','Wyoming');
insert into PickLists (PickListName, Code, Description) values ('State', 'YT','Yukon Territory');
go

/* Country                                                                                    */
insert into PickLists (PickListName, Code, Description) values ('Country', 'AF','Afghanistan');
insert into PickLists (PickListName, Code, Description) values ('Country', 'AR','Argentina');
insert into PickLists (PickListName, Code, Description) values ('Country', 'AU','Australia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'AT','Austria');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BH','Bahrain');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BD','Bangladesh');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BY','Belarus');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BE','Belgium');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BM','Bermuda');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BO','Bolivia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BA','Bosnia and Herzegowina');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BR','Brazil');
insert into PickLists (PickListName, Code, Description) values ('Country', 'BG','Bulgaria');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CA','Canada');
insert into PickLists (PickListName, Code, Description) values ('Country', 'TD','Chad');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CL','Chile');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CN','China');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CO','Colombia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CR','Costa Rica');
insert into PickLists (PickListName, Code, Description) values ('Country', 'HR','Croatia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CU','Cuba');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CZ','Czech Republic');
insert into PickLists (PickListName, Code, Description) values ('Country', 'DK','Denmark');
insert into PickLists (PickListName, Code, Description) values ('Country', 'DO','Dominican Republic');
insert into PickLists (PickListName, Code, Description) values ('Country', 'EG','Egypt');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SV','El Salvador');
insert into PickLists (PickListName, Code, Description) values ('Country', 'FI','Finland');
insert into PickLists (PickListName, Code, Description) values ('Country', 'FR','France');
insert into PickLists (PickListName, Code, Description) values ('Country', 'DE','Germany');
insert into PickLists (PickListName, Code, Description) values ('Country', 'GR','Greece');
insert into PickLists (PickListName, Code, Description) values ('Country', 'GL','Greenland');
insert into PickLists (PickListName, Code, Description) values ('Country', 'GT','Guatemala');
insert into PickLists (PickListName, Code, Description) values ('Country', 'HT','Haiti');
insert into PickLists (PickListName, Code, Description) values ('Country', 'HN','Honduras');
insert into PickLists (PickListName, Code, Description) values ('Country', 'HK','Hong Kong');
insert into PickLists (PickListName, Code, Description) values ('Country', 'HU','Hungary');
insert into PickLists (PickListName, Code, Description) values ('Country', 'IS','Iceland');
insert into PickLists (PickListName, Code, Description) values ('Country', 'IN','India');
insert into PickLists (PickListName, Code, Description) values ('Country', 'ID','Indonesia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'IR','Iran');
insert into PickLists (PickListName, Code, Description) values ('Country', 'IQ','Iraq');
insert into PickLists (PickListName, Code, Description) values ('Country', 'IE','Ireland');
insert into PickLists (PickListName, Code, Description) values ('Country', 'IL','Israel');
insert into PickLists (PickListName, Code, Description) values ('Country', 'IT','Italy');
insert into PickLists (PickListName, Code, Description) values ('Country', 'JM','Jamaica');
insert into PickLists (PickListName, Code, Description) values ('Country', 'JP','Japan');
insert into PickLists (PickListName, Code, Description) values ('Country', 'JO','Jordan');
insert into PickLists (PickListName, Code, Description) values ('Country', 'KE','Kenya');
insert into PickLists (PickListName, Code, Description) values ('Country', 'KP','North Korea');
insert into PickLists (PickListName, Code, Description) values ('Country', 'KR','South Korea');
insert into PickLists (PickListName, Code, Description) values ('Country', 'KW','Kuwait');
insert into PickLists (PickListName, Code, Description) values ('Country', 'LB','Lebanon');
insert into PickLists (PickListName, Code, Description) values ('Country', 'LR','Liberia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'LU','Luxembourg');
insert into PickLists (PickListName, Code, Description) values ('Country', 'MY','Malaysia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'MX','Mexico');
insert into PickLists (PickListName, Code, Description) values ('Country', 'MN','Mongolia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'MA','Morocco');
insert into PickLists (PickListName, Code, Description) values ('Country', 'NP','Nepal');
insert into PickLists (PickListName, Code, Description) values ('Country', 'NL','Netherlands');
insert into PickLists (PickListName, Code, Description) values ('Country', 'NZ','New Zealand');
insert into PickLists (PickListName, Code, Description) values ('Country', 'NI','Nicaragua');
insert into PickLists (PickListName, Code, Description) values ('Country', 'NG','Nigeria');
insert into PickLists (PickListName, Code, Description) values ('Country', 'NO','Norway');
insert into PickLists (PickListName, Code, Description) values ('Country', 'OM','Oman');
insert into PickLists (PickListName, Code, Description) values ('Country', 'PK','Pakistan');
insert into PickLists (PickListName, Code, Description) values ('Country', 'PA','Panama');
insert into PickLists (PickListName, Code, Description) values ('Country', 'PE','Peru');
insert into PickLists (PickListName, Code, Description) values ('Country', 'PH','Philippines');
insert into PickLists (PickListName, Code, Description) values ('Country', 'PL','Poland');
insert into PickLists (PickListName, Code, Description) values ('Country', 'PT','Portugal');
insert into PickLists (PickListName, Code, Description) values ('Country', 'PR','Puerto Rico');
insert into PickLists (PickListName, Code, Description) values ('Country', 'QA','Qatar');
insert into PickLists (PickListName, Code, Description) values ('Country', 'RO','Romania');
insert into PickLists (PickListName, Code, Description) values ('Country', 'RU','Russian Federation');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SA','Saudi Arabia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SG','Singapore');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SK','Slovakia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SB','Solomon Islands');
insert into PickLists (PickListName, Code, Description) values ('Country', 'ZA','South Africa');
insert into PickLists (PickListName, Code, Description) values ('Country', 'ES','Spain');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SD','Sudan');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SE','Sweden');
insert into PickLists (PickListName, Code, Description) values ('Country', 'CH','Switzerland');
insert into PickLists (PickListName, Code, Description) values ('Country', 'SY','Syria');
insert into PickLists (PickListName, Code, Description) values ('Country', 'TW','Taiwan');
insert into PickLists (PickListName, Code, Description) values ('Country', 'TH','Thailand');
insert into PickLists (PickListName, Code, Description) values ('Country', 'TR','Turkey');
insert into PickLists (PickListName, Code, Description) values ('Country', 'UG','Uganda');
insert into PickLists (PickListName, Code, Description) values ('Country', 'UA','Ukraine');
insert into PickLists (PickListName, Code, Description) values ('Country', 'AE','United Arab Emirates');
insert into PickLists (PickListName, Code, Description) values ('Country', 'GB','United Kingdom');
insert into PickLists (PickListName, Code, Description) values ('Country', 'US','United States');
insert into PickLists (PickListName, Code, Description) values ('Country', 'UY','Uruguay');
insert into PickLists (PickListName, Code, Description) values ('Country', 'VE','Venezuela');
insert into PickLists (PickListName, Code, Description) values ('Country', 'VN','Viet Nam');
insert into PickLists (PickListName, Code, Description) values ('Country', 'VG','British Virgin Islands');
insert into PickLists (PickListName, Code, Description) values ('Country', 'VI','U.S. Virgin Islands');
insert into PickLists (PickListName, Code, Description) values ('Country', 'YE','Yemen');
insert into PickLists (PickListName, Code, Description) values ('Country', 'YU','Yugoslavia');
insert into PickLists (PickListName, Code, Description) values ('Country', 'ZW','Zimbabwe');
go

/* YesNo */
insert into PickLists (PickListName, Code, Description) values ('YesNo', '1', 'Yes');
insert into PickLists (PickListName, Code, Description) values ('YesNo', '0', 'No');
go

/* Load CUSTOMERS data */
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'ALLENIND0001', 'Allen Industries', 'North', 'Mr. James Samek', 'President and CEO', 'Main Office', '(508) 555-2599', '(508) 555-1290', 'jsamek@allenindustries.com', 'www.allenindustries.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'FIRSTNAT0001', 'First National Bank', 'North', 'Mr. Milton Smith', 'Executive Vice President', 'Main', '(802) 921-4362', '(802) 921-8743', null, 'www.firstnationalbank.com', 'Retail', 0, 20000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'STATEREA0001', 'State Realty', 'North', 'Ms. Maria DeMayo', 'Chief Financial Officer', 'Primary', '(212) 521-8879', '(212) 521-0012', null, 'www.staterealty.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'TRIDENTI0002', 'Trident Inc.', 'North', 'Ms. Janine Marchand', 'Vice President', 'Main', '(603) 555-6348', '(603) 555-3443', null, 'www.trident.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'STUARTSE0001', 'Stuart''s Electronic Repair', 'North', 'Mr. Stuart R. Schroeder', 'President', 'HQ', '(508) 233-8712', '(508) 233-6521', null, 'www.stuartselectronicrepair.com', 'Retail', 0, 20000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'ABERDEEN0001', 'Aberdeen Inc.', 'North', 'Mr. Harold Michaelson', 'President', 'Main', '(617) 832-7300', '(617) 832-9863', null, 'www.aberdeen.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'TECHMANA0001', 'Tech Management', 'North', 'Mr. Lawrence Jackson', 'Vice President of MIS', 'Primary', '(738) 555-4783', '(738) 555-1237', null, 'www.techmanagement.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'SYMBOLEX0001', 'Symbolex', 'North', 'Mr. Curtis Simms', 'President', 'Main', '(212) 832-9876', '(212) 832-7676', 'csimms@symbolex.com', 'www.symbolex.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'KINGSTON0001', 'Kingston Manufacturing', 'North', 'Mr. William Wintringham', 'Vice President of Finance', 'Main', '(617) 553-2398', '(617) 553-1541', null, 'www.kingstonmanufacturing.com', 'Retail', 0, 25000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'TRIADBUS0001', 'Triad Business Centers', 'North', 'Mr. John Flowers', 'President', 'HQ', '(617) 823-3276', '(617) 823-8558', null, 'www.triadbusinesscenters.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'STEALTHE0001', 'Stealth Environmental Consulting', 'North', 'Mr. David Weeks', 'Chief Financial Officer', 'Main', '(617) 455-2211', '(617) 455-9099', null, 'www.stealthenvironmentalconsulting.com', 'Retail', 0, 15000, 'Net15' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'HANOVERE0001', 'Hanover Electronics', 'North', 'Mr. Martin Bailey', 'President', 'Main', '(603) 555-4122', '(603) 643-2233', null, 'www.hanoverelectronics.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'USDEVELO0001', 'United Systems Development', 'North', 'Mr. Ray Shadow', 'Operations Manager', 'Main', '(603) 234-6354', '(603) 234-9111', null, 'www.unitedsystemsdevelopment.com', 'Retail', 0, 40000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'THERMOCO0001', 'Thermocouple Inc.', 'North', 'Ms. Adrienne Doorman', 'Assistant Vice President', 'HQ', '(617) 555-8900', '(617) 555-3452', null, 'www.thermocouple.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'TANBERGI0001', 'Tanberg Industries', 'North', 'Mr. Howard Thorne', 'President', 'Primary', '(603) 555-3856', '(603) 555-2333', null, 'www.tanbergindustries.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'PESTONIM0001', 'Peston Importers', 'North', 'Mr. Phillip Leiss', 'President', 'Primary', '(603) 653-7734', '(603) 653-8219', null, 'www.pestonimporters.com', 'Wholesale', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'UNLIMITE0001', 'Unlimited Office Furniture', 'North', 'Mr. Reginald Archibald', 'President', 'Main', '(203) 555-9246', '(203) 555-6237', null, 'www.unlimitedofficefurniture.com', 'Retail', 0, 15000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'MAXWANCO0001', 'Maxwan Corporation', 'North', 'Mr. George Cocci', 'President', 'Main', '(603) 555-0955', '(603) 555-8777', null, 'www.maxwan.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'JOHNSONM0001', 'Johnson and Murcheson', 'North', 'Mr. Ed Johnson', 'Senior Vice President', 'Main', '(617) 434-7233', '(617) 434-7666', null, 'www.johnsonandmurcheson.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'LOMACREA0001', 'Lo-Mac Realty', 'North', 'Ms. Lois MacIntyre', 'Executive Vice President', 'Main', '(603) 221-0028', '(603) 221-3942', null, 'www.lomacrealty.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'TOURAINE0001', 'Touraine Inc.', 'North', 'Mr. John Errico', 'Vice President of Marketing', 'HQ', '(617) 683-7328', '(617) 384-9932', 'jerrico@touraine.com', 'www.touraine.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'SHANGMAI0001', 'Shang Mai Industries', 'North', 'Mr. Tom Yiung Jr.', 'Operations Manager', 'HQ', '(603) 885-9281', '(603) 992-0933', null, 'www.shangmaiindustries.com', 'Retail', 0, 10000, 'Net15' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'HANNAFOR0001', 'Hanna Ford Inc.', 'North', 'Mr. Franklin Hannaford', 'President', 'Primary', '(204) 987-2351', '(204) 987-4527', null, 'www.hannaford.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'HIGHTECH0001', 'High Technology Corporation', 'North', 'Mr. Joseph Barreca', 'Advertising Manager', 'Main', '(617) 838-5557', '(617) 838-8111', null, 'www.hightechnology.com', 'Retail', 0, 20000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'STRIKERI0001', 'Striker Industrial Supplies', 'North', 'Mr. John F Anderson', 'President', 'Main', '(603) 555-7657', '(603) 555-1121', null, 'www.strikerindustrialsupplies.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'CONSOLID0001', 'Consolidated Marketing Services Inc.', 'North', 'Mrs. Sandra Joyce', 'Chief Financial Officer', 'HQ', '(413) 945-3328', '(413) 945-0082', null, 'www.consolidatedmarketingservices.com', 'Retail', 0, 0, 'COD' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'STEVENSO0001', 'The Stevenson Company Inc.', 'North', 'Dr. Bruce Stevenson', 'President', 'Primary', '(203) 555-6780','(203) 555-0099', 'bstevenson@stevenson.com', 'www.stevenson.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'BEAUMONT0001', 'The Beaumont Company', 'North', 'Mr. Raymond Stowell', 'Assistant Vice President', 'Main', '(603) 993-0364','(603) 993-6578', null, 'www.beaumont.com', 'Retail', 0, 10000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'SWENSONC0001', 'Swenson Consulting', null, null, null, 'Main', '(617) 276-8890', '(617) 276-8873', null, 'www.swensonconsulting.com', 'Retail', 0, 10000, 'Net45' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'LITWAREI0001', 'Litware Incorporated', 'Mid-south', 'Mr. Jay Adams', null, 'Main', '(281) 555-0135','(281) 555-1224', 'JAdams@Litware.com', 'www.litware.com', 'Retail', 0, 20000, 'Net30' );
insert into dbo.Customers ( CustomerNumber, CompanyName, Region, ContactName, ContactTitle, PrimaryAddrName, Phone, Fax, Email, WebsiteURL, PriceList, CreditOnHold, CreditAmount, PaymentTerms ) values ( 'SPECIALT0001', 'Specialty Sports', 'Mid-south', 'Mr. Scott Konersmann', null, 'Main Office', '(214) 555-0135', '(214) 555-2313', 'SKonersmann@specialtysports.com', 'www.specialtysports.com', 'Retail', 0, 25000, 'Net45' );
go


/* Load ADDRESSES data */
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'ALLENIND0001', 'Main Office', 'Main', 'Mr. James K. Samek', 'President', '200 Blue Hills Avenue','Suite 2100','North Andover','MA','01845', null, '(508) 555-2599', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'ALLENIND0001', 'ShipTo1', 'ShipTo', 'Mr. David Lewis', 'MIS Manager', 'P.O. Box 2100', null, 'North Andover', 'MA', '01845', null, '(617) 555-2345', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'ALLENIND0001', 'ShipTo2', 'ShipTo', 'Mr. Barry Mullens', 'Operations Manager', '200 Blue Hills Avenue', 'Suite 2100', 'North Andover', 'MA', '01845', null, '(617) 555-2876', null, 'UPS-2day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'FIRSTNAT0001', 'Main', 'Main', 'Mr. Milton Smith', 'Executive Vice President', 'Winter Street', null, 'Auburn', 'MA', '01284', null, '(802) 921-4362', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'FIRSTNAT0001', 'ShipTo', 'ShipTo', 'Mr. Robert Duffield', 'MIS Manager', 'P.O. Box 2', null, 'Auburn', 'MA', '01284', null, null, null, 'UPS-2day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STATEREA0001', 'Primary', 'Main', 'Mr. Kevin Fitzgerald', 'Executive Vice President', '153 State Street', null, 'Princeton', 'NJ', '10948', null, '(212) 521-8879', null, 'FedEx-1day', 'ST-NJ' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'TRIDENTI0002', 'Main', 'Main', 'Ms. Janine Marchand', 'Vice President', '43 East Pine Street', null, 'Red Hook', 'NY', '03104', null, '(603) 555-6348', null, 'UPS-Ground', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STUARTSE0001', 'HQ', 'Main', 'Mr. Stuart R. Schroeder', 'President', '707 Dartmouth Street', null, 'Binghamton', 'NY', '01930', null, '(508) 233-8712', null, 'UPS-Ground', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STUARTSE0001', 'ShipTo', 'ShipTo', 'Mr. Frederick Greene', 'Operations Manager', '707 Dartmouth Street', null, 'Binghamton', 'NY', '01930', null, null, null, 'FedEx-1day', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'ABERDEEN0001', 'Main', 'Main', 'Mr. Harold V. Michaelson', 'President', '209 Narroway Road', null, 'Quincy', 'MA', '03927', null, '(617) 832-7300', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'ABERDEEN0001', 'ShipTo', 'ShipTo', 'Ms. Lois Anders', 'Operations Manager', 'P.O. Box 854', null, 'Quincy', 'MA', '03927', null, '(617) 683-7349', null, 'UPS-2day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'TECHMANA0001', 'Primary', 'Main', 'Mr. Lawrence Jackson', 'Vice President of MIS', '1254 Just Street', 'Suite 200', 'Atlantis', 'NJ', '34558', null, '(738) 555-4783', null, 'UPS-Ground', 'ST-NJ' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'SYMBOLEX0001', 'Main', 'Main', 'Mr. Curtis Simms', 'President', 'Stark Street', null, 'New York', 'NY', '10021', null, '(212) 832-9876', null, 'FedEx-1day', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'SYMBOLEX0001', 'ShipTo', 'ShipTo', 'Ms. Stacey Walsh', 'Systems Analyst', 'P.O. Box 777', null, 'New York', 'NY', '10021', null, null, null, 'FedEx-1day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'KINGSTON0001', 'Main', 'Main', 'Mr. William Wintringham', 'Vice President of Finance', '62 Boston Street', null, 'Lynn', 'MA', '01905', null, '(617) 553-2398', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'KINGSTON0001', 'ShipTo', 'ShipTo', 'Mr. Michael Walsh', 'Data Processing Manager', '62 Boston Street', null, 'Lynn', 'MA', '01905', null, null, null, 'FedEx-2day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'TRIADBUS0001', 'HQ', 'Main', 'Mr. John Flowers', 'President', 'P.O. Box 732', '132 Franklin Street', 'Boston', 'MA', '02116', null, '(617) 823-3276', null, 'FedEx-1day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STEALTHE0001', 'Main', 'Main', 'Mr. David Weeks', 'Chief Financial Officer', '62 Harvard Road', null, 'Quincy', 'MA', '02118', null, '(617) 455-2211', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STEALTHE0001', 'ShipTo', 'ShipTo', 'Ms. Susan Dolan', 'Vice President of Engineering', '62 Harvard Road', null, 'Quincy', 'MA', '02118', null, null, null, 'UPS-2day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'HANOVERE0001', 'Main', 'Main', 'Mr. Martin Bailey', 'President', '3 Webster Avenue', null, 'Hanover', 'NH', '03755', null, null, null, 'FedEx-1day', null );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'USDEVELO0001', 'Main', 'Main', 'Mr. Ray Shadow', 'Operations Manager', 'P.O. Box 666', null, 'Albany', 'NY', '03104', null, null, null, 'UPS-Ground', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'THERMOCO0001', 'HQ', 'Main', 'Ms. Adrienne Doorman', 'Assistant VP', '123 Federal Street', null, 'Boston', 'MA', '02115', null, '(617) 555-8900', null, 'FedEx-1day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'TANBERGI0001', 'Primary', 'Main', 'Mr. Howard Thorne', null, '110 River Road', null, 'Bedford', 'NH', '03102', null, '(603) 555-3856', null, 'FedEx-1day', 'ST-NH' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'TANBERGI0001', 'ShipTo', 'ShipTo', 'Ms. Kate Bars', 'Director of Operations', 'P.O. Box 3', null, 'Bedford', 'NH', '03102', null, null, null, 'UPS-Ground', null );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'PESTONIM0001', 'Primary', 'Main', null, null, 'Olde Lantern Road', 'Professional Office Park', 'Buffalo', 'NY', '03773', null, null, null, 'UPS-2day', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'UNLIMITE0001', 'Main', 'Main', 'Mr. Reginald Archibald', 'President', '109 Pleasant Street', null, 'Claremont', 'NH', '03743', null, '(203) 555-9246', null, 'FedEx-2day', null );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'MAXWANCO0001', 'Main', 'Main', 'Mr. George Cocci', 'President', '11 Cannon Street', 'Third Floor', 'Pittsburgh', 'PA', '03051', null, '(603) 555-0955', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'JOHNSONM0001', 'Main', 'Main', 'Mr. Edward Johnson', 'Senior Vice President', '62 Ocean Way', null, 'Boston', 'MA', '02111', null, '(617) 434-7233', null, 'UPS-2day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'LOMACREA0001', 'Main', 'Main', 'Ms. Lois MacIntyre', 'Executive Vice President', '527 Lincoln Ave.', null, 'Manchester', 'NH', '03101', null, '(603) 221-0028', null, 'FedEx-1day', 'ST-NH' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'TOURAINE0001', 'HQ', 'Main', 'Mr. John Errico', 'VP of Marketing', '140 Broadway', null, 'Everett', 'MA', '09218', null, '(617) 683-7328', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'SHANGMAI0001', 'HQ', 'Main', 'Mr. Tom Yiung Jr.', 'Operations Manager', '7731 Granite Way', null, 'Merrimack', 'NH', '03054', null, '(603) 885-9281', null, 'FedEx-1day', 'ST-NH' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'HANNAFOR0001', 'Primary', 'Main', 'Mr. Franklin Hannaford', 'President', 'P.O. Box 2356', 'Hartley Street', 'Rockingham', 'NY', '03266', null, '(204) 987-2351', null, 'UPS-Ground', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'HANNAFOR0001', 'ShipTo', 'ShipTo', 'Ms. April Clancey', 'MIS Manager', '1 Hartley Street', null, 'Rockingham', 'NY', '03266', null, '(206) 544-0901', null, 'FedEx-1day', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'HIGHTECH0001', 'Main', 'Main', 'Mr. Joseph Barreca', 'Advertising Manager', '120 Speen Street', null, 'Natick', 'MA', '01760', null, '(617) 838-5557', null, 'UPS-2day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STRIKERI0001', 'Main', 'Main', 'Mr. John F. Anderson', 'President', '321 Union Street', 'Suite 55', 'Portsmouth', 'NH', '03060', null, '(603) 555-7657', null, 'UPS-Ground', 'ST-NH' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STRIKERI0001', 'ShipTo', 'ShipTo', 'Mr. Steven J. Boomer', 'Plant Manager', '321 Union Street', 'Suite 55', 'Portsmouth', 'NH', '03060', null, null, null, 'FedEx-1day', 'ST-NH' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'CONSOLID0001', 'HQ', 'Main', 'Mrs. Sandra Joyce', 'Chief Financial Officer', 'P.O. Box 1234', '860 Route 3A', 'Amherst', 'MA', '03230', null, '(413) 945-3328', null, 'UPS-Ground', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STEVENSO0001', 'Primary', 'Main', 'Dr. Bruce Stevenson', 'President', 'P.O. Box 101', '123 Biology Drive', 'Hamden', 'NY', '06601-9391', null, '(203) 555-6780', null, 'FedEx-1day', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STEVENSO0001', 'ShipTo', 'ShipTo', 'Mr. Kevin Heslin', 'Vice President of MIS', '123 Dos Drive', null, 'Hamden', 'NY', '06601-9391', null, null, null, 'UPS-Ground', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'STEVENSO0001', 'BillTo', 'BillTo', 'Ms. Kay Towers', 'Vice President', 'P.O. Box 101', '123 Biology Drive', 'Hamden', 'NY', '06601-9391', null, null, null, 'FedEx-1day', 'ST-NY' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'BEAUMONT0001', 'Main', 'Main', 'Mr. Raymond Stowell', 'Assistant Vice President', 'P.O. Box 686', '200 Commercial Street', 'Manchester', 'NH', '03130', null, '(603) 993-0364', null, 'UPS-Ground', null );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'BEAUMONT0001', 'ShipTo', 'ShipTo', 'Mr. Paul Gerry', 'Data Processing Manager', '250 Commerical Street', null, 'Manchester', 'NH', '03130', null, null, null, 'FedEx-1day', 'ST-NH' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'SWENSONC0001', 'Main', 'Main', null, null, '2323 Brookline Drive', 'Suite 700', 'Boston', 'MA','02134', null, null, null, 'FedEx-1day', 'ST-MA' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'LITWAREI0001', 'Main', 'Main', 'Mr. Jay Adams', null, '990 Mountain Hill Boulevard', null, 'Houston', 'TX', '97300', null, '(281) 555-0135', null, 'FedEx-1day', 'ST-TX' );
insert into dbo.Addresses ( CustomerNumber, LocationName, AddressType, ContactName, ContactTitle, AddressLine1, AddressLine2, City, State, PostalCode, Country, Phone, Fax, ShippingMethod, TaxSchedule ) values ( 'SPECIALT0001', 'Main Office', 'Main', 'Scott Konersmann', null, '45678 78th Street', null, 'Dallas', 'TX','95486', null, '(214) 555-0135', null, 'FedEx-2day', 'ST-TX' );
go


/* Load PRODUCTS data */
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'LP970', 'Laser Printer', 'FinishGood', 'General', 425.00, 279.98, 275.35, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'SC610', 'Scanner', 'FinishGood', 'General', 225.00, 89.98, 91.00, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'ZD250', 'Zip Drive', 'FinishGood', 'General', 99.95, 35.98, 34.90, 15, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'DC315', 'Digital Camera', 'FinishGood', 'General', 169.95, 67.48, 69.25, 20, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'SW155', 'Software Bundle Home', 'FinishGood', 'General', 125.00, 44.98, 44.98, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'SW255', 'Software Bundle Office', 'FinishGood', 'General', 295.00, 112.48, 112.48, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'HD720', 'Hard Drive', 'Part', 'General', 225.00, 89.98, 91.00, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'ME128', '128MB Memory', 'Part', 'General', 84.95, 33.73, 34.25, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'ME256', '256MB Memory', 'Part', 'General', 169.95, 67.48, 68.40, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'MN015', '15in Monitor', 'FinishGood', 'General', 129.95, 53.98, 54.50, 12, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'MN017', '17in Monitor', 'FinishGood', 'General', 174.95, 71.98, 73.25, 13, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'MN019', '19in Monitor', 'FinishGood', 'General', 225.00, 89.98, 88.25, 18, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'SW101', 'Standard Warranty', 'Service', 'Warranty', 0, 0, 0, 0, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'EW102', 'Extended Warranty', 'Service', 'Warranty', 199.95, 0, 0, 0, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'CD032', '32x CD-ROM', 'Part', 'General', 84.95, 35.98, 34.95, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'CD048', '48x CD-ROM', 'Part', 'General', 109.95, 44.98, 45.95, 30, 2, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'SC300', 'Sound Card', 'Part', 'General', 44.95, 17.98, 18.25, 12, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'SP056', 'Speakers', 'FinishGood', 'General', 54.95, 18.98, 17.45, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'KB412', 'Keyboard', 'FinishGood', 'General', 29.95, 11.23, 11.50, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'MS501', 'Mouse', 'FinishGood', 'General', 22.50, 8.98, 9.25, 97, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'VC645', 'Video Card', 'Part', 'General', 74.95, 31.48, 32.50, 10, 3, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'DVD20', 'DVD Drive', 'Part', 'General', 249.95, 101.23, 102.50, 10, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'CAT5CBL', 'Cat5 Cable', 'FinishGood', 'Length', 0.75, 0.23, 0.22, 9783, 0, 0 );
insert into dbo.Products ( ProductNumber, ProductName, Type, UoMSchedule, ListPrice, Cost, StandardCost, QuantityInStock, QuantityOnOrder, Discontinued ) values ( 'CONSULT', 'Technical Consulting', 'Service', 'Consulting', 150.00, 0, 0, 0, 0, 0 );
go


/* Load PRODUCTPRICELISTS data */
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'LP970', 'Retail', 'Each', 399.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'LP970', 'Wholesale', 'Each', 329.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'SC610', 'Retail', 'Each', 199.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'ZD250', 'Retail', 'Each', 79.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'DC315', 'Retail', 'Each', 149.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'SW155', 'Retail', 'Each', 99.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'SW255', 'Retail', 'Each', 249.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'HD720', 'Retail', 'Each', 199.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'ME128', 'Retail', 'Each', 74.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'ME256', 'Retail', 'Each', 149.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'MN015', 'Retail', 'Each', 119.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'MN017', 'Retail', 'Each', 159.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'MN019', 'Retail', 'Each', 199.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'SW101', 'Retail', '1 Year', 0, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'EW102', 'Retail', '3 Year', 199.95, 3 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CD032', 'Retail', 'Each', 79.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CD048', 'Retail', 'Each', 99.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'SC300', 'Retail', 'Each', 39.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'SP056', 'Retail', 'Each', 19.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'SP056', 'Retail', 'Pair', 44.95, 2 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'KB412', 'Retail', 'Each', 24.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'MS501', 'Retail', 'Each', 19.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'MS501', 'Retail', 'Case', 189.95, 10 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'VC645', 'Retail', 'Each', 69.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'DVD20', 'Retail', 'Each', 224.95, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CAT5CBL', 'Retail', 'Foot', 0.70, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CAT5CBL', 'Retail', 'Yard', 1.95, 3 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CAT5CBL', 'Retail', 'Spool', 49.95, 100 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CONSULT', 'Retail', 'Hour', 150.00, 1 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CONSULT', 'Retail', 'Day', 1200.00, 8 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CONSULT', 'Retail', 'Week', 5500.00, 40 );
insert into dbo.ProductPriceLists ( ProductNumber, PriceList, UnitOfMeasure, UnitPrice, BaseUoMQuantity ) values ( 'CONSULT', 'Retail', 'Month', 20000.00, 176 );
go


/* Load ORDER_HEADER data */
insert into dbo.SalesOrders ( OrderNumber, CustomerNumber, BillToAddrName, Type, Status, OrderDate, SalespersonID, ShippedDate, ShippingMethod, PriceList, PaymentTerms, Freight, Discount, Tax, TotalAmount, RequestedShipDate, ShipContact, ShipAddressLine1, ShipAddressLine2, ShipCity, ShipState, ShipPostalCode, ShipCountry, ShipPhone, TaxSchedule, OriginalOrderNumber ) values ( 'ORD3000', 'ALLENIND0001', 'Main Office', 'Order', 'Closed', '2004-08-20', 'JAC', '2004-08-22', 'UPS-2day', 'Retail', 'Net30', 10.0000, 0.0000, 0.0000, 351.1000, '2004-08-21', 'Mr. Barry Mullens', '200 Blue Hills Avenue', 'Suite 2300', 'North Andover', 'MA', '01845', NULL, '(617) 555-2876', 'ST-MA', NULL );
insert into dbo.SalesOrders ( OrderNumber, CustomerNumber, BillToAddrName, Type, Status, OrderDate, SalespersonID, ShippedDate, ShippingMethod, PriceList, PaymentTerms, Freight, Discount, Tax, TotalAmount, RequestedShipDate, ShipContact, ShipAddressLine1, ShipAddressLine2, ShipCity, ShipState, ShipPostalCode, ShipCountry, ShipPhone, TaxSchedule, OriginalOrderNumber ) values ( 'INV3000', 'ALLENIND0001', 'Main Office', 'Invoice', 'Open', '2004-08-23', 'JAC',  '2004-08-22', 'UPS-2day', 'Retail', 'Net30', 10.0000, 0.0000, 0.0000, 351.1000, '2004-08-21', 'Mr. Barry Mullens', '200 Blue Hills Avenue', 'Suite 2300', 'North Andover', 'MA', '01845', NULL, '(617) 555-2876', 'ST-MA', 'ORD3000' );
insert into dbo.SalesOrders ( OrderNumber, CustomerNumber, BillToAddrName, Type, Status, OrderDate, SalespersonID, ShippedDate, ShippingMethod, PriceList, PaymentTerms, Freight, Discount, Tax, TotalAmount, RequestedShipDate, ShipContact, ShipAddressLine1, ShipAddressLine2, ShipCity, ShipState, ShipPostalCode, ShipCountry, ShipPhone, TaxSchedule, OriginalOrderNumber ) values ( 'ORD3001', 'ALLENIND0001', 'Main Office', 'Order', 'Draft', '2004-08-23', 'JAC', NULL, 'UPS-2day', 'Retail', 'Net30', 20.0000, 0.0000, 0.0000, 665.6000, '2004-09-10', 'Mr. Barry Mullens', '200 Blue Hills Avenue', 'Suite 2300', 'North Andover', 'MA', '01845', NULL, '(617) 555-2876', 'ST-MA', NULL );
insert into dbo.SalesOrders ( OrderNumber, CustomerNumber, BillToAddrName, Type, Status, OrderDate, SalespersonID, ShippedDate, ShippingMethod, PriceList, PaymentTerms, Freight, Discount, Tax, TotalAmount, RequestedShipDate, ShipContact, ShipAddressLine1, ShipAddressLine2, ShipCity, ShipState, ShipPostalCode, ShipCountry, ShipPhone, TaxSchedule, OriginalOrderNumber ) values ( 'ORD3002', 'STEVENSO0001', 'BillTo', 'Order', 'Open', '2004-08-23', 'JAC', NULL, 'UPS-Ground', 'Retail', 'Net30', 15.0000, 0.0000, 0.0000, 1117.2400, NULL, 'Mr. Kevin Heslin', '123 Dos Drive', NULL, 'Hamden', 'NY', '06601-9391', NULL, NULL, 'ST-NY', NULL );
go


/* Load ORDER_DETAIL data */
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'ORD3000', 1, 1, 'DC315', 'Digital Camera', 'Each', 2, 149.9500, .0000, 14.9950, 314.9000, NULL, NULL, NULL );
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'ORD3000', 2, 1, 'KB412', 'Keyboard', 'Each', 1, 24.9500, .0000, 1.2475, 26.2000, NULL, NULL, NULL );
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'INV3000', 1, 1, 'DC315', 'Digital Camera', 'Each', 2, 149.9500, .0000, 14.9950, 314.9000, NULL, NULL, NULL );
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'INV3000', 2, 1, 'KB412', 'Keyboard', 'Each', 1, 24.9500, .0000, 1.2475, 26.2000, NULL, NULL, NULL );
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'ORD3001', 1, 1, 'DVD20', 'DVD Drive', 'Each', 1, 224.9500, 10.0000, 10.7475, 225.7000, '2004-09-17 00:00:00.000', NULL, NULL );
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'ORD3001', 2, 1, 'MN019', '19in Monitor', 'Each', 2, 199.9500, .0000, 19.9950, 419.9000, '2004-09-10 00:00:00.000', NULL, NULL );
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'ORD3002', 1, 1, 'HD720', 'Hard Drive', 'Each', 3, 199.9500, .0000, 29.9925, 629.8400, NULL, NULL, NULL );
insert into dbo.SalesOrderDetails ( OrderNumber, LineNumber, InventoryItem, ProductNumber, Description, UnitOfMeasure, Quantity, UnitPrice, ItemDiscount, ItemTax, ExtendedPrice, RequestedShipDate, QuantityShipped, QuantityCancelled ) values ( 'ORD3002', 2, 1, 'DVD20', 'DVD Drive', 'Each', 2, 224.9500, .0000, 22.4950, 472.4000, NULL, NULL, NULL );
go


/* Load GENERALLEDGER data */
insert into GeneralLedger (BatchID, JournalEntry, Reference, TransactionDate, Description, DebitAmount, DebitAccountString, Description2, CreditAccountString, CreditAmount, UserID) values('Scenario10', '0098765432', 'Reference', '4/13/07', 'The Debit', '200', '000-1100-00', 'The Credit', '000-1210-00', '200', 'sa');
insert into GeneralLedger (BatchID, JournalEntry, Reference, TransactionDate, Description, DebitAmount, DebitAccountString, Description2, CreditAccountString, CreditAmount, UserID) values('Scenario10', '0098765433', 'Reference2', '4/13/07', 'Debit2', '500', '000-1100-00', 'Credit2', '000-1210-00', '500', 'sa');
go
