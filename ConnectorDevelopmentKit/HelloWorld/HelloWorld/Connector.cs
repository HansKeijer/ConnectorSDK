﻿using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Core.ConnectorApi.Common;
using Scribe.Core.ConnectorApi.ConnectionUI;
using Scribe.Core.ConnectorApi.Query;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    [ScribeConnector(
        ConnectorSettings.ConnectorTypeId,
        ConnectorSettings.Name,
        ConnectorSettings.Description,
        typeof(Connector),
        StandardConnectorSettings.SettingsUITypeName,
        StandardConnectorSettings.SettingsUIVersion,
        StandardConnectorSettings.ConnectionUITypeName,
        StandardConnectorSettings.ConnectionUIVersion,
        StandardConnectorSettings.XapFileName,
        new[] { "Scribe.IS.Source", "Scribe.IS.Target", "Scribe.IS2.Source", "Scribe.IS2.Target" },
        ConnectorSettings.SupportsCloud, ConnectorSettings.ConnectorVersion)]
    public class Connector : IConnector
    {
        private string username;
        public void Connect(IDictionary<string, string> properties)
        {
            this.username = properties["Username"];
            this.IsConnected = true;
        }

        public Guid ConnectorTypeId
        {
            get { return new Guid(ConnectorSettings.ConnectorTypeId); }
        }

        public void Disconnect()
        {
            this.IsConnected = false;
        }

        public MethodResult ExecuteMethod(MethodInput input)
        {
            throw new NotImplementedException();
        }

        public OperationResult ExecuteOperation(OperationInput input)
        {
            var name = input.Name;

            // We are not implementing BULK
            DataEntity data = input.Input[0];

            // We do not require a lookup condition, but if you did, this is where you could get it.
            Expression lookup = input.LookupCondition[0];

            // Most Connectors will want to dispatch using the entity name, but we only have the one.
            var entityName = data.ObjectDefinitionFullName;

            switch (name)
            {
                case MetadataProvider.UpsertActionName:

                    // string greetingTemplate
                    var template = input.Input[0].Properties[MetadataProvider.GreetingTemplatePropertyName].ToString();

                    // Perform the actual operation, just updates a template
                    var storage = new LocalDataStorage();
                    storage.WriteData(MetadataProvider.GreetingTemplatePropertyName + this.username, template);

                    return new OperationResult
                    {
                        ErrorInfo = new ErrorResult[] { null },
                        ObjectsAffected = new[] { 1 },
                        Output = new[] { data },
                        Success = new[] { true }
                    };
            }

            return new OperationResult
            {
                ErrorInfo = new[] { new ErrorResult { Number = 1, Description = string.Format("Operation '{0}' is not supported.", name), Detail = string.Empty } },
                ObjectsAffected = new[] { 0 },
                Output = null,
                Success = new[] { false }
            };
        }

        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            var storage = new LocalDataStorage();
            var greetingTemplate = storage.ReadData(MetadataProvider.GreetingTemplatePropertyName + this.username);

            if (string.IsNullOrWhiteSpace(greetingTemplate)) { greetingTemplate = "Hello world!"; }


            yield return new DataEntity(MetadataProvider.GreetingEntityName)
            {
                Children = new EntityChildren(),
                Properties = new EntityProperties { { MetadataProvider.MessagePropertyName, string.Format(greetingTemplate, this.username) } }
            };
        }

        private MetadataProvider metadataProvider = new MetadataProvider();
        public IMetadataProvider GetMetadataProvider()
        {
            return this.metadataProvider;
        }

        public bool IsConnected { get; set; }

        public string PreConnect(IDictionary<string, string> properties)
        {
            var form = new FormDefinition
            {
                CompanyName = "Scribe Software",
                CryptoKey = "1",
                HelpUri = new Uri("http://www.scribesoft.com"),
                Entries =
                    new Collection<EntryDefinition>
                            {
                                new EntryDefinition
                                    {
                                        InputType = InputType.Text,
                                        IsRequired = true,
                                        Label = "User name",
                                        PropertyName = "Username"
                                    }
                            }
            };

            return form.Serialize();
        }
    }
}
