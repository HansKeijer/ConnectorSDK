﻿namespace HelloWorld
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Scribe.Core.ConnectorApi;
    using Scribe.Core.ConnectorApi.Metadata;

    public class MetadataProvider : IMetadataProvider
    {
        public const string GreetingEntityName = "Greeting";

        public const string GreetingTemplatePropertyName = "GreetingTemplate";

        public const string MessagePropertyName = "Message";

        public const string QueryActionName = "Query";

        public const string UpsertActionName = "Upsert";

        public const string UserNamePropertyName = "Username";

        private IEnumerable<IActionDefinition> actionDefinitions;

        private IEnumerable<IObjectDefinition> objectDefinitions;

        private IEnumerable<IActionDefinition> ActionDefinitions { get { return this.actionDefinitions ?? (this.actionDefinitions = this.GetActionDefinitions()); } }

        private IEnumerable<IObjectDefinition> ObjectDefinitions { get { return this.objectDefinitions ?? (this.objectDefinitions = this.GetObjectDefinitions()); } }

        private IEnumerable<IActionDefinition> GetActionDefinitions()
        {
            return new List<IActionDefinition>
                   {
                       new ActionDefinition
                       {
                           Description = "Queries stuff",
                           FullName = QueryActionName,
                           KnownActionType = KnownActions.Query,
                           Name = QueryActionName,
                           SupportsBulk = false,
                           SupportsConstraints = true,
                           SupportsInput = false,
                           SupportsLookupConditions = true,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = false,
                           SupportsSequences = false
                       },
                       new ActionDefinition
                       {
                           Description = "Sets stuff",
                           FullName = UpsertActionName,
                           KnownActionType = KnownActions.UpdateInsert,
                           Name = UpsertActionName,
                           SupportsBulk = false,
                           SupportsConstraints = false,
                           SupportsInput = true,
                           SupportsLookupConditions = true,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = false,
                           SupportsSequences = false
                       }
                   };
        }

        private IEnumerable<IObjectDefinition> GetObjectDefinitions()
        {
            return new List<IObjectDefinition>
                   {
                       new ObjectDefinition
                       {
                           FullName = GreetingEntityName,
                           Description = "Provides a hello world like greeting.",
                           Hidden = false,
                           Name = GreetingEntityName,
                           SupportedActionFullNames = new List<string>{QueryActionName, UpsertActionName},
                           PropertyDefinitions = new List<IPropertyDefinition>
                                                 {
                                                     new PropertyDefinition
                                                     {
                                                         Description = "Message for greeting.",
                                                         FullName = MessagePropertyName,
                                                         IsPrimaryKey = false,
                                                         MaxOccurs = 1,
                                                         MinOccurs = 0,
                                                         Name = MessagePropertyName,
                                                         Nullable = true,
                                                         NumericPrecision = 0,
                                                         NumericScale = 0,
                                                         PresentationType = "string",
                                                         PropertyType = typeof(string).Name,
                                                         UsedInActionInput = false,
                                                         UsedInLookupCondition = false,
                                                         UsedInQueryConstraint = false,
                                                         UsedInActionOutput = false,
                                                         UsedInQuerySelect = true,
                                                         UsedInQuerySequence = false
                                                     },
                                                     new PropertyDefinition
                                                     {
                                                         Description = "Template for greeting.",
                                                         FullName = GreetingTemplatePropertyName,
                                                         IsPrimaryKey = false,
                                                         MaxOccurs = 1,
                                                         MinOccurs = 0,
                                                         Name = GreetingTemplatePropertyName,
                                                         Nullable = true,
                                                         NumericPrecision = 0,
                                                         NumericScale = 0,
                                                         PresentationType = "string",
                                                         PropertyType = typeof(string).Name,
                                                         UsedInActionInput = true,
                                                         UsedInLookupCondition = false,
                                                         UsedInQueryConstraint = false,
                                                         UsedInActionOutput = false,
                                                         UsedInQuerySelect = false,
                                                         UsedInQuerySequence = false
                                                     }
                                                 }
                       }
                   };
        }

        public void ResetMetadata()
        {
        }

        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            return this.ActionDefinitions;
        }

        public IMethodDefinition RetrieveMethodDefinition(string objectName, bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IObjectDefinition RetrieveObjectDefinition(string objectName, bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return this.ObjectDefinitions.First(od => od.Name == objectName);
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return this.ObjectDefinitions;
        }

        public void Dispose()
        {
        }
    }
}