﻿namespace YourCompany.Connector.ScribeOrganizations
{
    /// <summary>
    /// Used for Discovery. These constants are used to fill in some of the values in the
    /// ScribeConnectorAttrribute declaration on the class that implements IConnector.
    /// Connector settings that are often or must different from other connectors.
    /// </summary>
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "64f397f2-0860-429a-8e00-947c19c3045c";
        public const string ConnectorVersion = "1.0";
        public const string Description = "Connector for Scribe Online Orgs.";
        public const string Name = "ScribeOrgs";
        public const bool SupportsCloud = true;
        public const string CompanyName = "YourCompany";
        public const string AppName = "ScribeOrganizations";
        public const string Copyright = "Copyright © 2015 YourCompany All rights reserved.";
    }
}
