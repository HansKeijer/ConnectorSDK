﻿namespace YourCompany.Connector.ScribeOrganizations.Models
{

    public class Organization
    {
        public int id { get; set; }
        public string name { get; set; }
        // public string[] administrators { get; set; }
        public int parentId { get; set; }
        public string website { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public string primaryContactFirstName { get; set; }
        public string primaryContactLastName { get; set; }
        public string primaryContactEmail { get; set; }
        public string primaryContactPhoneNumber { get; set; }
        public string primaryContactStreet { get; set; }
        public string primaryContactCity { get; set; }
        public string primaryContactState { get; set; }
        public string primaryContactPostalCode { get; set; }
        public string primaryContactCountry { get; set; }
        public bool isSourceDataLocal { get; set; }
        // public Securityrule[] securityRules { get; set; }
        public string apiToken { get; set; }
        public bool isAgentLogDownloadAllowed { get; set; }
    }

}
