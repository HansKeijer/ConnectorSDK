﻿namespace MyCompany.MessagingConnector.stripdemo
{
    using Simple.Connector.Framework;

    using System;
    using System.Collections.Generic;

    using Models;

    [MessagingConnector(Settings.Id, Settings.Name, typeof(Connector), Settings.Version)]
    public class Connector : JsonMessagingConnectorBase
    {
        public Connector() : base(new Guid(Settings.Id), Settings.CompanyName, Settings.HelpUrl) { }

        protected override IDictionary<string, MessageDescription> RegisterEntities()
        {
            return this.Start.Register<Rootobject, Customer>(ro => ro.data._object);
        }
    }
}