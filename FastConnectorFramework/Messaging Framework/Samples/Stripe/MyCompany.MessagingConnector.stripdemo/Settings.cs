﻿namespace MyCompany.MessagingConnector.stripdemo
{
    public static class Settings
    {
        public const string Id = "7875f118-9ea8-494f-bfc6-6f11cdf95496";
        public const string Name = "Stripe";
        public const string CompanyName = "Scribe";
        public const string HelpUrl = "http://www.scribesoft.com";
        public const string Version = "1.0.0.0";
    }
}
